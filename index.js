var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);

var mongoose = require('mongoose');
mongoose.connect('mongodb://socketio:socketio@ds021884.mlab.com:21884/socketio');

var chatObj = require('./model.js');

app.use(express.static(path.join(__dirname, 'public')));

/** Function to decode image  */
function decodeBase64Image(dataString) {
     var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
     response = {};

     if (matches.length !== 3) {
          return new Error('Invalid input string');
     }

     response.type = matches[1];
     response.data = new Buffer(matches[2], 'base64');

     return response;
}
/** -----------End --------------*/


app.get('/', function(req, res){
     res.render('index.html');
});

io.on('connection', function(socket){

     /** code for saving file into the mongodb **/
     socket.on('sendFile', function(bufferData){

          var mimeType = decodeBase64Image(bufferData).type;
          var imgExt = mimeType.split('/')[1];
          var base64Data = bufferData.replace(/^data:image\/(png|gif|jpeg);base64,/,'');
          var uploadImgName = new Date().getTime() + "." + imgExt;

          // obj for chat model
          var saveObj = new chatObj;
          saveObj.img.data = new Buffer(base64Data, 'base64');
          saveObj.img.mimeType = mimeType;
          saveObj.img.name = uploadImgName;
          saveObj.save(function (err, mediaObj) {
               if (err) throw err;
               io.emit('receiveFile', { image: true, buffer: mediaObj.img.data.toString('base64'), mimeType : mediaObj.img.mimeType});
          })
     });


     /** code for writing file into the directory **/

     /**socket.on('sendFile', function(bufferData){
          var mimeType = decodeBase64Image(bufferData).type;
          var imgExt = mimeType.split('/')[1];

          var base64Data = bufferData.replace(/^data:image\/(png|gif|jpeg);base64,/,'');
          var uploadImg = new Date().getTime() + "." + imgExt;
          fs.writeFile(uploadImg, base64Data, 'base64', function(err) {
                 if (err) console.log(err);

                 fs.readFile(uploadImg, function (err, imageBuffer) {
                        io.emit('receiveFile', { image: true, buffer: imageBuffer.toString('base64'), mimeType : mimeType});
                 })
          });
     }); */
      /** -------------------- End ------------*/
});

http.listen(4000, function(){
     console.log('listening on *:4000');
});
